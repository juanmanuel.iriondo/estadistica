# Practica de Big Data de Estadística

Esta es mi práctica de Estadística.
El notebook está en el enlace de abajo. En el propio fichero está explicado todo lo que voy haciendo y las respuestas a las preguntas.
Espero haberme explicado bien. Cualquier duda, por favor, me lo comentas.

## Enlace al notebook de Jupyter PracticaFinal-JuanManuelIriondo.ipynb

https://drive.google.com/file/d/1Dct0jve3_SNbMIHnI7LSmTFHfjlwV1jJ/view?usp=sharing
